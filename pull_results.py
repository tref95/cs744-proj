import boto3
import os

if not os.path.exists("RES"):
    os.makedirs("RES")

bucket_name = "cs744-g18"

s3=boto3.client('s3')
list=s3.list_objects(Bucket=bucket_name)['Contents']
for key in list:
    s3.download_file(bucket_name, key['Key'], "RES/"+key['Key'])