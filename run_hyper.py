import subprocess
import socket
import boto3
from botocore.exceptions import ClientError
import logging
import numpy

def upload_file(file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True

# Argument names can be found in "lstm_twitter"
# [Min, Max (inclusive), Step]
lstm_twitter_params = {
    "batch-size": [16, 256, 32],
    "buckets": [100, 10000, 500],
    "samples": [.01, .8, .05],
    "embed-dim": [112, 512, 32],
    "lstm-out": [112, 512, 32],
    "dropout": [.1, .9, .05]
}

for key in lstm_twitter_params.keys():
    for i in numpy.arange(lstm_twitter_params[key][0], lstm_twitter_params[key][1]+lstm_twitter_params[key][2], lstm_twitter_params[key][2]):
        # pid = subprocess.Popen(["iostat", "-w", "1"], stdout=open(f'{key}-{i}-{socket.gethostname()}-metrics.out', "wb"),
        #                        stderr=subprocess.STDOUT)
        # DSTAT only works on linux where installed, but has best results
        pidssh = subprocess.Popen(["ssh", "node1", 'cd ~/cs744-proj ; python3 client_hyper.py --index 1 --' + key + ' '+str(i)])
        # pidssh.stdin.write()
        pid = subprocess.Popen(["dstat", "-t", "-c", "-d", "-m", "-s", "-n"],
                               stdout=open(f'lstm-{key}-{i}-node0-metrics.out', "wb"),
                               stderr=subprocess.STDOUT)
        subprocess.call(["python3", "lstm_twitter_distributed.py", "--"+key, str(i), "--index", str(0)], stdout=open('tmpfile.out', 'wb'))
        pid.terminate()
        pidssh.terminate()
        subprocess.call(["ssh", "node1", 'pkill python'])
        subprocess.call(["ssh", "node1", 'pkill dstat'])
        subprocess.call(["ssh", "node1", 'pkill ssh'])
        subprocess.call(["scp", f'node1:~/cs744-proj/lstm-{key}-{i}-node1-metrics.out', "."])
        fin = open('tmpfile.out', "r")
        data = fin.read()
        fin.close()
        fout = open(f'lstm-{key}-{i}-node0-metrics.out', 'a')
        fout.write("####OUTPUT####\n")
        fout.write(data)
        fout.close()
        upload_file(f'lstm-{key}-{i}-node0-metrics.out', 'cs744-g18')
        upload_file(f'lstm-{key}-{i}-node1-metrics.out', 'cs744-g18')
