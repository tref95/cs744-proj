from os import listdir
from os.path import isfile, join
from out_to_dataframe import out_to_dataframe
import matplotlib.pyplot as plt
import pandas as pd

#################################
# Define lambda function for byte mapping.
byte_mapping = {"B": "0", "k": "000", "M": "000000", "G": "0000000000"}
def map_bytes(x):
    for byte, number in byte_mapping.items():
        x = x.replace(byte, number)
    return x
#################################

# Prefix to the files we want to get.
model_par = "lstm-batch-size"
model_name = model_par.split('-')[0]
model_param = '-'.join(model_par.split('-')[1:])

# Change file path to where RES data is stored.
mypath = "C:\\Users\\jeff\\Documents\\CS744FinalProject\\RES"
files = [f for f in listdir(mypath) if isfile(join(mypath, f))]

relfiles = [s for s in files if model_par in s]

df_dicts = {}
for file in relfiles:
    df_dicts[file.split('-ip')[0]] = out_to_dataframe(mypath, file)

df_dict_keys = df_dicts[relfiles[0].split('-ip')[0]].keys()

# Set up a dictionary for each type of resource.
df_resources = {}
for dict_key in df_dict_keys:
    df_resources[dict_key] = {}
    for dict in df_dicts:
        df_dict = df_dicts[dict]
        df_resources[dict_key][dict] = df_dict[dict_key]

##################
# Edit below to get graphs for each resource_col tuple given.
# data.columns = ["time", "cpu", "disk", "memory", "swap", "network"]
resource_cols = [('cpu', 'usr'), ('memory', 'used')]
# Denote a reduce_factor to get less of the files. Set to -1 to get all.
reduce_factor = 32
##################

for (resource, col) in resource_cols:
    df = pd.DataFrame()
    i = 0
    for df_dict in df_dicts:
        df_col = df_resources[resource][df_dict][col]
        df_col = df_col.astype('str')
        df_col = df_col.apply(lambda x: map_bytes(x))
        df_col = df_col.astype('double')
        param_val = df_dict.split("-")[-1]
        if reduce_factor != -1 and int(param_val) % reduce_factor == 0:
            df.insert(i, int(param_val), df_col, True)
            i += 1

    df = df.reindex(sorted(df.columns), axis=1)
    df.plot()
    plt.title(model_name + " " + model_param + ": " + str(resource) +
              " (" + str(col) + ")")
    plt.legend(loc=4)
    plt.show()
