import subprocess
import socket
import boto3
from botocore.exceptions import ClientError
import logging
import numpy

def upload_file(file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True

# Argument names can be found in "svm_twitter"
# [Min, Max (inclusive), Step]
svm_twitter_params = {
    "estimators": [1, 25, 1],
    "jobs": [1, 8, 1]
}
for typek in ["linear", "poly", "rbf"]:
    for key in svm_twitter_params.keys():
        for i in numpy.arange(svm_twitter_params[key][0], svm_twitter_params[key][1]+svm_twitter_params[key][2], svm_twitter_params[key][2]):
            # pid = subprocess.Popen(["iostat", "-w", "1"], stdout=open(f'{key}-{i}-{socket.gethostname()}-metrics.out', "wb"),
            #                        stderr=subprocess.STDOUT)
            # DSTAT only works on linux where installed, but has best results
            pid = subprocess.Popen(["dstat", "-t", "-c", "-d", "-m", "-s", "-n"],
                                   stdout=open(f'svm-samplelock-{typek}-{key}-{i}-{socket.gethostname()}-metrics.out', "wb"),
                                   stderr=subprocess.STDOUT)
            subprocess.call(["python3", "svm_bagged_twitter.py", "--"+key, str(i)], stdout=open('tmpfile.out', 'wb'))
            pid.terminate()
            fin = open('tmpfile.out', "r")
            data = fin.read()
            fin.close()
            fout = open(f'svm-samplelock-{typek}-{key}-{i}-{socket.gethostname()}-metrics.out', 'a')
            fout.write("####OUTPUT####\n")
            fout.write(data)
            fout.close()
            upload_file(f'svm-samplelock-{typek}-{key}-{i}-{socket.gethostname()}-metrics.out', 'cs744-g18')
