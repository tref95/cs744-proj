import pandas as pd
import re
import io


def out_to_dataframe(dir, input_file):
    samples = []
    append = False
    with open(dir + "\\" + input_file) as myfile:
        for line in myfile.readlines():
            if re.search(r'time', line):
                append = True
            elif re.search(r'####OUTPUT####', line):
                break
            if append:
                samples.append(line)

    data = pd.read_csv(io.StringIO('\n'.join(samples)), sep="|", header=None)
    data.columns = ["time", "cpu", "disk", "memory", "swap", "network"]

    resource_dict = {}
    for c in data.columns:
        resource_dict[c] = pd.DataFrame(data[c].copy()).copy()
        resource_dict[c] = resource_dict[c][c].str.split(expand=True)
        resource_dict[c].columns = resource_dict[c].iloc[0]
        if c == 'time':
            resource_dict[c].columns = ["day", "time"]
        resource_dict[c] = resource_dict[c][1:]

    return resource_dict

