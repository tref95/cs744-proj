import numpy as np
import pandas as pd
from sklearn.preprocessing import OneHotEncoder
from keras.models import Sequential
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import classification_report, confusion_matrix
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('--samples', help='Percentage of samples')

args = parser.parse_args()

# HYPERPARAMETERS.
# Samples should be set to full if possible.
# Percentage of samples to use from 100000 original samples.
samples = 0.05

# Interesting hyperparameters to edit.
#######################################
#######################################

if args.samples:
    samples = float(args.samples)

# test and validation size can be left as is.
# Percentage of the samples to use as the test set.
test_size = .2

# fix random seed for reproducibility
rand_num = np.random.seed(7)

# load the dataset.
dataframe = pd.read_json('RAW/parsed-tokenized.json')
dataset = pd.DataFrame(dataframe.values)
df = dataset.sample(frac=samples).reset_index(drop=True)
x = df[:][0]
y = df[:][1]
y = pd.DataFrame(y).astype(int)
words = len(x[1])
df = pd.DataFrame.from_records(x)
m, n = df.shape

df_max_value = df.max().max()
L = [list(range(df_max_value + 1))] * words

df_L = pd.DataFrame.from_records(L)
df_L = df_L.transpose()

enc = OneHotEncoder()
enc.fit(df_L)
onehotlabels = enc.transform(df).toarray()

X_train, X_test, Y_train, Y_test = train_test_split(onehotlabels, y, test_size=test_size, random_state=rand_num)
model = SVC(kernel='linear')
model.fit(X_train, Y_train)

Y_pred = model.predict(X_test)
print(confusion_matrix(Y_test, Y_pred))
print(classification_report(Y_test, Y_pred))