import json
import nltk
from nltk.corpus import stopwords
import re
from keras.preprocessing.sequence import pad_sequences

nltk.download('stopwords')

def clean_text(text):
    text = text.lower()  # lowercase text
    text = REPLACE_BY_SPACE_RE.sub(' ',
                                   text)  # replace REPLACE_BY_SPACE_RE symbols by space in text. substitute the matched string in REPLACE_BY_SPACE_RE with space.
    text = BAD_SYMBOLS_RE.sub('',
                              text)  # remove symbols which are in BAD_SYMBOLS_RE from text. substitute the matched string in BAD_SYMBOLS_RE with nothing.
    text = text.replace('x', '')
    #    text = re.sub(r'\W+', '', text)
    text = ' '.join(word for word in text.split() if word not in STOPWORDS)  # remove stopwors from text
    return text



REPLACE_BY_SPACE_RE = re.compile('[/(){}\[\]\|@,;]')
BAD_SYMBOLS_RE = re.compile('[^0-9a-z #+_]')
STOPWORDS = set(stopwords.words('english'))
# The maximum number of words to be used. (most frequent)
MAX_NB_WORDS = 50000
# Max number of words in each complaint.
MAX_SEQUENCE_LENGTH = 250
# This is fixed.
EMBEDDING_DIM = 100

data = []
with open('RAW/negative_tweets.json') as json_file:
    for line in json_file:
        tweet = json.loads(line)
        data.append({"body": clean_text(tweet['text']), "sentiment": 0})
with open('RAW/positive_tweets.json') as json_file:
    for line in json_file:
        tweet = json.loads(line)
        data.append({"body": clean_text(tweet['text']), "sentiment": 1})

with open('RAW/parsed.json', 'w') as f:
    json.dump(data, f)

with open('RAW/parsed.json') as json_file:
    data = json.load(json_file)
    dictionary = {}
    ret_data = []
    for row in data:
        words = row["body"].split()
        arr = []
        for word in words:
            if dictionary.get(word) != None:
                arr.append(dictionary.get(word))
            else:
                dictionary[word] = len(dictionary)
                arr.append(dictionary.get(word))
        ret_data.append(arr)
    ret_data = pad_sequences(ret_data)
    for index, row in enumerate(data):
        row["body"] = ret_data[index].tolist()
    with open('RAW/parsed-tokenized.json', 'w') as f:
        json.dump(data, f)
