import numpy as np
import pandas as pd
from sklearn.preprocessing import OneHotEncoder
from keras.models import Sequential
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D
from sklearn.model_selection import train_test_split
import argparse
import sys

parser=argparse.ArgumentParser()

parser.add_argument('--buckets', help='Number of buckets')
parser.add_argument('--batch-size', help='Batch size')
parser.add_argument('--samples', help='Percentage of samples')
parser.add_argument('--embed-dim', help='Batch size')
parser.add_argument('--lstm-out', help='Size of LSTM layer')
parser.add_argument('--dropout', help='LSTM Dropout')

args = parser.parse_args()

print(args)

# HYPERPARAMETERS.
# p and samples should be set to full if possible.
# Hash the words into p buckets. There are 21154 words.
# Set p = 0 to have no hashing occur.
p = 10
# Percentage of samples to use from 100000 original samples.
samples = .02

# Interesting hyperparameters to edit.
#######################################
# Size of the embedded LSTM layer.
embed_dim = 256
# Size of the output from the LSTM layer.
lstm_out = 196
# Dropout for LSTM.
dropout = .2
# Recurrent dropout for LSTM
recurrent_dropout = .2
# Batch size for training.
batch_size = 32
# Number of epochs.
epochs = 5
#######################################

if args.buckets:
    p = int(args.buckets)
if args.batch_size:
    batch_size = int(args.batch_size)
if args.embed_dim:
    embed_dim = int(args.embed_dim)
if args.samples:
    samples = float(args.samples)
if args.lstm_out:
    lstm_out = int(args.lstm_out)
if args.dropout:
    dropout = float(args.dropout)

# test and validation size can be left as is.
# Percentage of the samples to use as the test set.
test_size = .2

rand_num = np.random.seed(7)

# load the dataset.
dataframe = pd.read_json('RAW/parsed-tokenized.json')
dataset = pd.DataFrame(dataframe.values)
df = dataset.sample(frac=samples).reset_index(drop=True)
x = df[:][0]
y = df[:][1]
y = y.apply(lambda i: (.99999 if i else 0))
y = pd.DataFrame(y)
words = len(x[1])
df_enc_train = pd.DataFrame
df = pd.DataFrame.from_records(x)
if p != 0:
    df = df.apply(lambda x: x.apply(lambda y: hash(y) % p), axis=1)
m, n = df.shape

df_max_value = df.max().max()
L = [list(range(df_max_value + 1))] * words

df_L = pd.DataFrame.from_records(L)
df_L = df_L.transpose()

enc = OneHotEncoder()
enc.fit(df_L)
onehotlabels = enc.transform(df).toarray()

model = Sequential()
model.add(Embedding(df_max_value+1, embed_dim, input_length=onehotlabels.shape[1]))
model.add(SpatialDropout1D(0.4))
model.add(LSTM(lstm_out, dropout=dropout, recurrent_dropout=recurrent_dropout))
model.add(Dense(2, activation='softmax'))
model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
print(model.summary())
print(model.count_params())

X_train, X_test, Y_train, Y_test = train_test_split(onehotlabels, y, test_size=test_size, random_state=rand_num)
model.fit(X_train, Y_train, epochs=epochs, batch_size=batch_size, verbose=2)

score, acc = model.evaluate(X_test, Y_test, verbose=2, batch_size=batch_size)
print("score: %.2f" % (score))
print("acc: %.2f" % (acc))
