import numpy as np
import pandas as pd
from sklearn.preprocessing import OneHotEncoder
from keras.models import Sequential
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.ensemble import RandomForestClassifier
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('--samples', help='Percentage of samples')
parser.add_argument('--jobs', help='Number of jobs to run in parallel')
parser.add_argument('--estimators', help='Trees in forest')
parser.add_argument('--leaf', help='Min Samples per leaf')
parser.add_argument('--bootstrap', help='bootstrap sample')

args = parser.parse_args()


# HYPERPARAMETERS.
# Samples should be set to full if possible.
# Percentage of samples to use from 100000 original samples.
samples = 1.0

# Interesting hyperparameters to edit.
#######################################
# Number of jobs to run in parallel.
# A value of -1 defaults to all available processors.
n_jobs = -1

# The number of trees in the forest.
n_estimators = 10

# The minimum number of samples required to be at a leaf node.
# A split point at any depth will only be considered if it leaves at least min_samples_leaf training samples in each of
# the left and right branches. This may have the effect of smoothing the model, especially in regression.
min_samples_leaf = 20

# Whether to bootstrap sample from dataset or not.
bootstrap = False
#######################################

if args.samples:
    samples = float(args.samples)
if args.estimators:
    n_estimators = int(args.estimators)
if args.jobs:
    n_jobs = int(args.jobs)
if args.leaf:
    min_samples_leaf = int(args.leaf)

# test and validation size can be left as is.
# Percentage of the samples to use as the test set.
test_size = .2

# fix random seed for reproducibility
rand_num = np.random.seed(7)

# load the dataset.
dataframe = pd.read_json('RAW/parsed-tokenized.json')
dataset = pd.DataFrame(dataframe.values)
df = dataset.sample(frac=samples).reset_index(drop=True)
x = df[:][0]
y = df[:][1]
y = pd.DataFrame(y).astype(int)
words = len(x[1])
df = pd.DataFrame.from_records(x)
m, n = df.shape

df_max_value = df.max().max()
L = [list(range(df_max_value + 1))] * words

df_L = pd.DataFrame.from_records(L)
df_L = df_L.transpose()

enc = OneHotEncoder()
enc.fit(df_L)
onehotlabels = enc.transform(df).toarray()

X_train, X_test, Y_train, Y_test = train_test_split(onehotlabels, y, test_size=test_size, random_state=rand_num)
model = RandomForestClassifier(n_jobs=n_jobs, n_estimators=n_estimators, min_samples_leaf=min_samples_leaf,
                               bootstrap=bootstrap)
model.fit(X_train, Y_train)

Y_pred = model.predict(X_test)
print(confusion_matrix(Y_test, Y_pred))
print(classification_report(Y_test, Y_pred))