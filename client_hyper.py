import argparse
import subprocess
import socket
import boto3
from botocore.exceptions import ClientError
import logging
import numpy

def upload_file(file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True

index = 0

parser = argparse.ArgumentParser()

parser.add_argument('--buckets', help='Number of buckets')
parser.add_argument('--batch-size', help='Batch size')
parser.add_argument('--samples', help='Percentage of samples')
parser.add_argument('--embed-dim', help='Batch size')
parser.add_argument('--lstm-out', help='Size of LSTM layer')
parser.add_argument('--dropout', help='LSTM Dropout')
parser.add_argument('--index', help='Worker index')

args = parser.parse_args()

print(args)

# HYPERPARAMETERS.
# p and samples should be set to full if possible.
# Hash the words into p buckets. There are 21154 words.
# Set p = 0 to have no hashing occur.
p = 100
# Percentage of samples to use from 100000 original samples.
samples = .05

# Interesting hyperparameters to edit.
#######################################
# Size of the embedded LSTM layer.
embed_dim = 256
# Size of the output from the LSTM layer.
lstm_out = 196
# Dropout for LSTM.
dropout = .2
# Recurrent dropout for LSTM
recurrent_dropout = .2
# Batch size for training.
batch_size = 32
# Number of epochs.
epochs = 5
#######################################

key = ""

if args.buckets:
    key = "buckets"
    p = int(args.buckets)
    keyval = p
if args.batch_size:
    key = "batch-size"
    batch_size = int(args.batch_size)
    keyval = batch_size
if args.embed_dim:
    key = "embed-dim"
    embed_dim = int(args.embed_dim)
    keyval = embed_dim
if args.samples:
    key = "samples"
    samples = float(args.samples)
    keyval = samples
if args.lstm_out:
    key = "lstm-out"
    lstm_out = int(args.lstm_out)
    keyval = lstm_out
if args.dropout:
    key = "dropout"
    dropout = float(args.dropout)
    keyval = dropout
if args.index:
    index = int(args.index)

# pid = subprocess.Popen(["iostat", "-w", "1"], stdout=open(f'{key}-{i}-{socket.gethostname()}-metrics.out', "wb"),
#                        stderr=subprocess.STDOUT)
# DSTAT only works on linux where installed, but has best results
pid = subprocess.Popen(["dstat", "-t", "-c", "-d", "-m", "-s", "-n"],
                       stdout=open(f'lstm-{key}-{keyval}-node{index}-metrics.out', "wb"),
                       stderr=subprocess.STDOUT)
subprocess.call(["python3", "lstm_twitter_distributed.py", "--"+key, str(keyval), "--index", str(index)], stdout=open('tmpfile.out','wb'))
pid.terminate()
# fin = open('tmpfile.out', "r")
# data = fin.read()
# fin.close()
# fout = open(f'lstm-{key}-{keyval}-node{index}-metrics.out', 'a')
# fout.write("####OUTPUT####\n")
# fout.write(data)
# fout.close()
# upload_file(f'lstm-{key}-{keyval}-node{index}-metrics.out', 'cs744-g18')
